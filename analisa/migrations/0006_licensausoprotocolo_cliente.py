# -*- coding: utf-8 -*-
from __future__ import unicode_literals
# Generated by Django 1.11 on 2018-01-27 16:50
from __future__ import unicode_literals

from django.db import migrations
import django.db.models.deletion
import djangoplus.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('analisa', '0005_auto_20180121_0835'),
    ]

    operations = [
        migrations.AddField(
            model_name='licensausoprotocolo',
            name='cliente',
            field=djangoplus.db.models.fields.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='analisa.Cliente', verbose_name='Cliente'),
        ),
    ]
