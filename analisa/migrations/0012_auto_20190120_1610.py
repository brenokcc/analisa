# Generated by Django 2.0.8 on 2019-01-20 16:10
from django.db import migrations
from django.conf import settings
import os


def unmigrate(apps, schema_editor):
    Processo = apps.get_model('analisa', 'Processo')
    Processo.objects.update(tamanho_arquivo=None)


def migrate(apps, schema_editor):
    Processo = apps.get_model('analisa', 'Processo')
    Processo.objects.filter(arquivo='').update(tamanho_arquivo=0)
    for processo in Processo.objects.exclude(arquivo=''):
        caminho_arquivo = os.path.join(settings.MEDIA_ROOT, processo.arquivo.name)
        if os.path.exists(caminho_arquivo):
            processo.tamanho_arquivo = os.stat(caminho_arquivo).st_size
        else:
            processo.tamanho_arquivo = 0
        processo.save()


class Migration(migrations.Migration):

    dependencies = [
        ('analisa', '0011_processo_tamanho_arquivo'),
    ]

    operations = [
        migrations.RunPython(migrate, unmigrate)
    ]
