# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os

def get_file_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (instance.uuid, ext)
    return os.path.join('processos', filename)


def fixBadZipfile(zipFile):  
    f = open(zipFile, 'r+b')  
    content = f.read()  
    pos = content.rfind('\x50\x4b\x05\x06') # reverse find: this string of bytes is the end of the zip's central directory.
    if pos>0:
        f.seek(pos+20) # +20: see secion V.I in 'ZIP format' link above.
        f.truncate()
        f.write('\x00\x00') # Zip file comment length: 0 byte length; tell zip applications to stop reading.
        f.close()
