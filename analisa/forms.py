# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from djangoplus.ui.components import forms
from analisa.models import Processo, Cliente, Pessoa, Recurso,\
    Licitacao, LicensaUsoProtocolo, Genero
import base64
from djangoplus.utils.formatter import format_value
from djangoplus.utils.dateutils import parse_date


class GerarChaveForm(forms.Form):
    class Meta:
        title = 'Gerar Chave'
        submit_label = 'Gerar Chave'

    cliente = forms.ModelChoiceField(Cliente.objects.all(), label='Cliente')
    validade = forms.DateField(label='Validade')

    def processar(self):
        return base64.encodestring(
            '%s:%s' % (self.cleaned_data['cliente'].pk, format_value(self.cleaned_data['validade']))
        )


class AtivacaoForm(forms.Form):
    class Meta:
        title = 'Ativar Protocolo'
        submit_label = 'Ativar'

    chave = forms.CharField(label='Chave')

    def clean_chave(self):
        try:
            cliente_id, validade = base64.decodestring(self.cleaned_data['chave']).split(':')
            self.cliente_id = cliente_id
            self.validade = parse_date(validade)
        except:
            raise forms.ValidationError('Chave inválida.')

    def processar(self):
        licensa = LicensaUsoProtocolo(validade=self.validade, cliente_id=self.cliente_id)
        licensa.save()


class LocalizarProcessoForm(forms.Form):

    cliente = forms.ModelChoiceField(Cliente.objects.all(), label='Cliente', required=False)
    objeto = forms.CharField(label='Objeto', required=False)

    ano = forms.ChoiceField(label='Ano', choices=[['', '']]+Processo.ANO_CHOICES, required=False)
    numero = forms.CharField(label='Número', required=False)
    credor = forms.ModelChoiceField(Pessoa.objects.all(), label='Credor', required=False, lazy=True)

    numero_empenho = forms.CharField(label='Número do Empenho', required=False)
    genero = forms.ModelChoiceField(queryset=Genero.objects.all(), label='Gênero', required=False, form_filters=[('cliente', 'cliente')])
    recurso = forms.ModelChoiceField(queryset=Recurso.objects.all(), label='Recurso', required=False, form_filters=[('cliente', 'cliente')])
    licitacao = forms.ModelChoiceField(queryset=Licitacao.objects.all(), label='Licitação', required=False, form_filters=[('cliente', 'cliente')])
    numero_pasta = forms.CharField(label='Localização Física', required=False)
    fieldsets = (('Palavras-Chaves', {'fields': ('cliente', ('ano', 'numero', 'credor'), ('genero', 'recurso', 'licitacao'), ('numero_empenho', 'objeto', 'numero_pasta'))}),)

    class Meta:
        method = 'get'
        submit_label = 'Buscar'

    def clean(self):

        qs = Processo.objects.all()
        if self.cleaned_data.get('cliente'):
            qs = qs.filter(cliente=self.cleaned_data.get('cliente'))
        if self.cleaned_data.get('objeto'):
            qs = qs.filter(ascii__icontains=self.cleaned_data.get('objeto'))
        if self.cleaned_data.get('ano'):
            qs = qs.filter(ano=self.cleaned_data.get('ano'))
        if self.cleaned_data.get('objeto'):
            qs = qs.filter(objeto__icontains=self.cleaned_data.get('objeto'))
        if self.cleaned_data.get('numero'):
            qs = qs.filter(numero=self.cleaned_data.get('numero'))
        if self.cleaned_data.get('credor'):
            qs = qs.filter(credor=self.cleaned_data.get('credor'))
        if self.cleaned_data.get('numero_empenho'):
            qs = qs.filter(numero_empenho__icontains=self.cleaned_data.get('numero_empenho'))
        if self.cleaned_data.get('genero'):
            qs = qs.filter(genero=self.cleaned_data.get('genero'))
        if self.cleaned_data.get('licitacao'):
            qs = qs.filter(licitacao=self.cleaned_data.get('licitacao'))
        if self.cleaned_data.get('recurso'):
            qs = qs.filter(recurso=self.cleaned_data.get('recurso'))
        if self.cleaned_data.get('numero_pasta'):
            qs = qs.filter(numero_pasta__icontains=self.cleaned_data.get('numero_pasta'))
        qs = qs.all(self.request.user)

        if qs.exists():
            self.qs = qs
        else:
            raise forms.ValidationError('Nenhum processo foi encontrado.')

    def processar(self):
        return self.qs


class ImportarProcessosForm(forms.Form):
    arquivo = forms.FileField(label='Arquivo')

    class Meta:
        title = 'Importação de Processos'
        submit_label = 'Importar'