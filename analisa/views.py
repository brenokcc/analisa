# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from djangoplus.decorators.views import view, action
from djangoplus.ui.components.navigation.breadcrumbs import httprr
from djangoplus.ui.components.paginator import Paginator
from analisa.forms import LocalizarProcessoForm, GerarChaveForm, AtivacaoForm
from analisa.models import Cliente


@view('Página Principal')
def index(request):
    #return httprr(request, '/list/analisa/processo/')
    if not request.user.is_authenticated:
        url = '/login/'
    elif request.user.groups.filter(name='Visualizador').exists():
        url = '/analisa/buscador/'
    else:
        url = '/list/analisa/processo/'
    return httprr(request, url)


@view('Buscador', icon='fa-search', menu='Buscador')
def buscador(request):
    title = 'Busca Avançada'
    form = LocalizarProcessoForm(request)
    for field_name in form.fields:
        form.fields[field_name].required = False
    if form.is_valid():
        qs = form.processar()
        paginator = Paginator(request, qs, 'Processos', search_fields=[], list_filter=[])
    return locals()


@view('Gerar Chave', menu='Gerar Chave de Protocolo', can_view=('Administrador',), icon='fa-key')
def gerar_chave(request):
    form = GerarChaveForm(request)
    if form.is_valid():
        chave = form.processar()
    return locals()


@view('Ativar Módulo de Protocolo', can_view=('Protocolo',), menu='Ativar Protocolo', icon='fa-check')
def ativar_modulo_protocolo(request):
    form = AtivacaoForm(request)
    if form.is_valid():
        form.processar()
        return httprr(request, '..', 'Ativação realizada com sucesso.')
    return locals()


@view('Download', can_view=('Administrador', 'Cadastrador'), icon='fa-download', menu='Download')
def download(request):
    title = 'Download'
    return locals()


@action(Cliente, 'Gerar Script de Exportação', can_execute=('Administrador', 'Cadastrador'), icon='fa-play', inline=True)
def gerar_scripts(request, pk):
    cliente = Cliente.objects.get(pk=pk)
    pks = cliente.gerar_escript_exportacao()
    nao_encontrados = cliente.processo_set.filter(pk__in=pks)
    return locals()
