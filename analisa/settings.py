# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from djangoplus.conf.base_settings import *
from os.path import abspath, dirname, join, exists
from os import sep

BASE_DIR = abspath(dirname(dirname(__file__)))
PROJECT_NAME = __file__.split(sep)[-2]

STATIC_ROOT = join(BASE_DIR, 'static')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': join(BASE_DIR, 'sqlite.db'),
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}

WSGI_APPLICATION = '%s.wsgi.application' % PROJECT_NAME

INSTALLED_APPS += (
    PROJECT_NAME,
    'endless',
    # 'djangoplus.ui.themes.default',
)

ROOT_URLCONF = '%s.urls' % PROJECT_NAME

if exists(join(BASE_DIR, 'logs')):
    DEBUG = False
    ALLOWED_HOSTS = ['*']
    HOST_NAME = '189.124.139.92:8000'
    SERVER_EMAIL = 'simop@analisarn.com.br'
    ADMINS = [('Admin', 'brenokcc@yahoo.com.br')]

    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
    EMAIL_USE_TLS = True
    EMAIL_HOST = 'smtp.gmail.com'
    EMAIL_HOST_USER = 'simop@analisarn.com.br'
    EMAIL_HOST_PASSWORD = 'simop123'
    EMAIL_PORT = 587

    MEDIA_ROOT = join('/mnt/AnalisaRN/media')
    DROPBOX_TOKEN = 'eHBQN7-ov-AAAAAAAAAAHJ5Xj44LMwiUBg0b-8BgWBO_ioGK3qTyKLgrFtaW80If'
else:
    EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
    EMAIL_FILE_PATH = join(BASE_DIR, 'mail')
    MEDIA_ROOT = join(BASE_DIR, 'media')

EXTRA_CSS = ['/static/override.css']
EXTRA_JS = ['/static/analisa.js']
USERNAME_MASK = '000.000.000-00'

DIGITAL_OCEAN_TOKEN = '64ad58f22ce2f94412845d3c1d19653a381ef51a650b7171a49720dfd1c2e48a'
DIGITAL_OCEAN_SERVER = '165.227.177.237'
DIGITAL_OCEAN_DOMAIN = ''
