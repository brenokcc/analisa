# -*- coding: utf-8 -*-
from analisa.models import Cliente
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def handle(self, *args, **options):
        for cliente in Cliente.objects.all():
            cliente.gerar_escript_exportacao()

