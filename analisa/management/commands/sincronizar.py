# -*- coding: utf-8 -*-
import os
from django.conf import settings
from analisa.models import Processo
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def handle(self, *args, **options):
        names = [line[77:].strip() for line in os.popen('cat /var/opt/analisa/.dropbox.log | grep success').read().split('\n')]
        tmp_dir = os.path.join(settings.BASE_DIR, '.dropbox')
        for processo in Processo.objects.exclude(arquivo__in=names)[0:100]:
            link_path = os.path.join(tmp_dir, processo.arquivo.name)
            if os.path.exists(processo.arquivo.path):
                os.symlink(processo.arquivo.path, link_path)
                print(processo.arquivo.path, link_path)


