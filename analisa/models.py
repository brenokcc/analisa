# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
import shutil
from analisa import utils
from django.utils.text import slugify
from django.core.exceptions import ValidationError
from django.db.models.aggregates import Sum
from django.http.response import HttpResponseRedirect
from djangoplus.db import models
from djangoplus.decorators import action, subset, meta, role
from djangoplus.admin.models import Unit
from zipfile import ZipFile
from django.conf import settings
from os import path
import datetime
import qrcode
import uuid
import zipfile
import os
from djangoplus.utils.storage.dropbox import DropboxStorage


class Pessoa(models.AsciiModel):
    nome = models.CharField('Nome', search=True)

    class Meta:
        verbose_name = 'Pessoa'
        verbose_name_plural = 'Pessoas'
        order_by = ('nome',)
        can_add = 'Cadastrador', 'Administrador'
        can_list = 'Cadastrador', 'Administrador', 'Visualizador', 'Protocolo', 'Mantenedor'

    def __str__(self):
        documento = ''
        if hasattr(self, 'pessoafisica'):
            documento = self.pessoafisica.cpf
        elif hasattr(self, 'pessoajuridica'):
            documento = self.pessoajuridica.cnpj
        return '%s %s' % (self.nome, documento and '(%s)' % documento or '')

    def get_documento(self):
        documento = None
        if hasattr(self, 'pessoafisica'):
            documento = self.pessoafisica.cpf
        elif hasattr(self, 'pessoajuridica'):
            documento = self.pessoajuridica.cnpj
        return documento


class PessoaFisica(Pessoa):
    cpf = models.CpfField('CPF', search=True)

    class Meta:
        verbose_name = 'Pessoa Física'
        verbose_name_plural = 'Pessoas Físicas'
        can_admin = 'Cadastrador', 'Administrador'
        can_add = 'Mantenedor'
        can_edit = 'Mantenedor'
        can_list = 'Mantenedor'
        menu = 'Pessoas Físicas', 'fa-user'
        icon = 'fa-user'

    def __str__(self):
        return '%s (%s)' % (self.nome, self.cpf)


class PessoaJuridica(Pessoa):
    cnpj = models.CnpjField('CNPJ', search=True)

    class Meta:
        verbose_name = 'Pessoa Jurídica'
        verbose_name_plural = 'Pessoas Jurídicas'
        can_admin = 'Cadastrador', 'Administrador'
        can_add = 'Mantenedor'
        can_edit = 'Mantenedor'
        can_list = 'Mantenedor'
        menu = 'Pessoas Jurídicas', 'fa-building'
        icon = 'fa-building'

    def __str__(self):
        return '%s (%s)' % (self.nome, self.cnpj)


@role('cpf')
class Administrador(models.Model):
    cpf = models.CpfField('CPF', search=True)
    nome = models.CharField('Nome', search=True)

    class Meta:
        verbose_name = 'Administrador'
        verbose_name_plural = 'Administradores'
        menu = 'Usuários::Administradores', 'fa-users'
        can_admin = 'Administrador'
        icon = 'fa-lock'

    def __str__(self):
        return self.nome


@role('cpf')
class Cadastrador(models.Model):
    cpf = models.CpfField('CPF', search=True)
    nome = models.CharField('Nome', search=True)

    class Meta:
        verbose_name = 'Cadastrador'
        verbose_name_plural = 'Cadastradores'
        menu = 'Usuários::Cadastradores', 'fa-users'
        can_admin = 'Administrador'
        icon = 'fa-user'

    def __str__(self):
        return self.nome


class Cliente(Unit):
    razao_social = models.CharField('Razão Social', search=True)
    cnpj = models.CnpjField('CNPJ', search=True)
    telefone = models.PhoneField('Telefone', blank=True)
    endereco = models.TextField('Endereço')
    nome_gestor = models.CharField('Nome')
    cpf_gestor = models.CpfField('CPF')
    telefone_gestor = models.PhoneField('Telefone', blank=True)

    fieldsets = (('Dados do Orgão', {'fields': ('razao_social', ('cnpj', 'telefone'), 'endereco')}),
                 ('Dados do Gestor', {'fields': ('nome_gestor', ('cpf_gestor', 'telefone_gestor'),)}),
                 ('Recursos::', {'relations': ('recurso_set',)}),
                 ('Licitações::', {'relations': ('licitacao_set',)}),
                 ('Gêneros::', {'relations': ('genero_set',)}),
                 ('Secretarias::', {'relations': ('secretaria_set',)}),
                 ('Usuários::Mantenedores', {'relations': ('mantenedor_set',)}),
                 ('Usuários::Protocolos', {'relations': ('protocolo_set',)}),
                 ('Usuários::Visualizadores', {'relations': ('visualizador_set',)}),
                 ('Licenças de Uso::Protocolo', {'relations': ('licensausoprotocolo_set',)}),
                 )

    class Meta:
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'
        menu = 'Clientes', 'fa-building'
        list_menu = 'Administrador', 'Cadastrador'
        list_display = ['razao_social', 'cnpj', 'qtd_processos', 'qtd_paginas', 'get_tamanhos_arquivos']
        can_admin = 'Cadastrador', 'Administrador'
        can_list_by_unit = 'Protocolo', 'Visualizador', 'Mantenedor'
        icon = 'fa-building'

    def __str__(self):
        return '%s (%s)' % (self.razao_social, self.cnpj)

    @meta('Processos')
    def qtd_processos(self):
        return self.processo_set.count()

    @meta('Páginas')
    def qtd_paginas(self):
        return self.processo_set.aggregate(Sum('numero_paginas')).get('numero_paginas__sum') or 0

    @meta('Armazenamento')
    def get_tamanhos_arquivos(self, recalcular=False):
        unidade = 'bytes'
        tamanho_arquivo = self.processo_set.aggregate(Sum('tamanho_arquivo')).get('tamanho_arquivo__sum') or 0
        if tamanho_arquivo > 1024:
            tamanho_arquivo = tamanho_arquivo / 1024
            unidade = 'Kb'
        if tamanho_arquivo > 1024:
            tamanho_arquivo = tamanho_arquivo / 1024
            unidade = 'Mb'
        if tamanho_arquivo > 1024:
            tamanho_arquivo = tamanho_arquivo / 1024
            unidade = 'Gb'
        return '{:.2f} {}'.format(tamanho_arquivo, unidade).replace('.', ',')

    def pode_usuar_protocolo(self):
        return self.licensausoprotocolo_set.filter(validade__gte=datetime.datetime.today()).exists()

    @action('Importar Processos', can_execute=('Administrador', 'Cadastrador'), inline=True, input='ImportarProcessosForm')
    def importar_processos(self, arquivo):
        data = dict()
        data[Pessoa] = dict()
        data[PessoaFisica] = dict()
        data[PessoaJuridica] = dict()
        data[Licitacao] = dict()
        data[Recurso] = dict()
        processos = []

        dir = path.join(settings.BASE_DIR, 'tmp')
        media_dir = path.join(settings.MEDIA_ROOT, 'processos')
        if not os.path.exists(dir):
            os.makedirs(dir)
        for f in os.listdir(dir):
            os.unlink(os.path.join( dir,f))

        zip_file = ZipFile(arquivo)
        zip_file.extractall(dir)
        json_file = open(path.join(dir, 'processos.json'), 'r')

        for tmp in Processo.objects.filter(cliente=self):
            tmp.delete()

        for item in json.loads(json_file.read().decode('iso-8859-1').encode('utf8')):
            p = Processo()

            documento_credor = item['documento_credor']
            nome_credor = item['nome_credor']

            if len(documento_credor) > 17:
                qs_pessoa = PessoaJuridica.objects.filter(cnpj=documento_credor)
                if qs_pessoa.exists():
                    credor = qs_pessoa[0]
                else:
                    credor = PessoaJuridica.objects.create(cnpj=documento_credor, nome=nome_credor)
            else:
                qs_pessoa = PessoaFisica.objects.filter(cpf=documento_credor)
                if qs_pessoa.exists():
                    credor = qs_pessoa[0]
                else:
                    credor = PessoaFisica.objects.create(cpf=documento_credor, nome=nome_credor)

            p.cliente = self
            if item['genero']:
                p.genero = Genero.objects.get_or_create(descricao=item['genero'], cliente=self)[0]
            p.objeto = item['objeto']
            if item['recurso']:
                p.recurso = Recurso.objects.get_or_create(descricao=item['recurso'], cliente=self)[0]
            p.credor = credor
            p.ano = int(item['ano'])
            p.numero = item['numero']
            p.numero_pasta = item['numero_pasta']
            if item['licitacao']:
                p.licitacao = Licitacao.objects.get_or_create(descricao=item['licitacao'], cliente=self)[0]
            p.mes = item['mes']
            p.numero_empenho = item['numero_empenho']
            if item['secretaria']:
                p.secretaria = Secretaria.objects.get_or_create(descricao=item['secretaria'], cliente=self)[0]
            p.numero_paginas = int(item['numero_paginas'])
            p.save()

            a = path.join(dir, '%s.pdf'%item['numero'].replace('/', '_'))
            b = path.join(media_dir, '%s.pdf' % p.uuid)
            c = 'processos/%s.pdf' % p.uuid

            Processo.objects.filter(pk=p.pk).update(arquivo=c)
            shutil.move(a, b)

        for f in os.listdir(dir):
            os.unlink(os.path.join(dir, f))

    @action('Exportar Processos', can_execute=('Administrador', 'Cadastrador'), inline=True, style='zip')
    def exportar_processos(self):
        file_path = os.path.join(settings.MEDIA_ROOT, 'processos.zip')
        l = []
        for processo in self.processo_set.all():
            item = dict(numero=processo.numero, ano=processo.ano, mes=processo.mes, secretaria=processo.secretaria and processo.secretaria.descricao or '', genero=processo.genero and processo.genero.descricao or '', objeto=processo.objeto, nome_credor=processo.credor.nome, documento_credor=processo.credor.get_documento(), numero_empenho=processo.numero_empenho, recurso=processo.recurso.descricao, licitacao=processo.licitacao.descricao, numero_pasta=processo.numero_pasta, numero_paginas=processo.numero_paginas)
            l.append(item)
        s = json.dumps(l)
        json_file_path = os.path.join(settings.MEDIA_ROOT, 'processos.json')
        json_file = open(json_file_path, 'w')
        json_file.write(s)
        json_file.close()
        z = zipfile.ZipFile(file_path, 'w', zipfile.ZIP_STORED)
        z.write(json_file_path, 'processos.json')
        for processo in self.processo_set.all():
            z.write(processo.arquivo.path, '%s.pdf' % processo.numero.replace('/', '_'))
            z.write(processo.qrcode.path, '%s.gif' % processo.numero.replace('/', '_'))
        z.close()
        # os.unlink(file_path)
        os.unlink(json_file_path)
        return HttpResponseRedirect('/media/processos.zip')

    def delete(self, *args, **kwargs):
        for processo in self.processo_set.all():
            processo.delete()
        super(Cliente, self).delete(*args, **kwargs)


    def gerar_escript_exportacao(self):
        nao_encontrados = []
        slug = slugify(self.razao_social)
        dados = []
        for model in [Recurso, Genero, Secretaria, Licitacao]:
            l = list()
            l.append('var {} = ['.format(model.__name__.upper()))
            for obj in model.objects.filter(cliente=self).values():
                if len(l) > 1: l.append(', ')
                l.append(json.dumps(obj))
            l.append('];')
            dados.append(''.join(l))

        l = list()
        l.append('var ANO = [')
        for ano in Processo.objects.filter(cliente=self).values_list('ano', flat=True).order_by('ano').distinct():
            if len(l) > 1: l.append(', ')
            l.append(json.dumps(dict(id=str(ano), text=str(ano))))

        l.append('];')
        dados.append(''.join(l))

        l = list()
        l.append('var PESSOA = [')
        pks = Processo.objects.filter(cliente=self).values_list('credor', flat=True).order_by('credor').distinct()
        for pessoa in Pessoa.objects.filter(pk__in=pks):
            if len(l) > 1: l.append(', ')
            l.append(json.dumps(dict(id=pessoa.pk, nome=pessoa.nome)))

        l.append('];')
        dados.append(''.join(l))

        copy_medias = []
        l = list()
        l.append('var PROCESSO = [')
        for obj in Processo.objects.filter(cliente=self).values():
            obj['credor'] = obj['credor_id'] and str(Pessoa.objects.get(pk=obj['credor_id'])) or '-'
            obj['secretaria'] = obj['secretaria_id'] and str(Secretaria.objects.get(pk=obj['secretaria_id'])) or '-'
            obj['genero'] = obj['genero_id'] and str(Genero.objects.get(pk=obj['genero_id'])) or '-'
            obj['recurso'] = obj['recurso_id'] and str(Recurso.objects.get(pk=obj['recurso_id'])) or '-'
            obj['licitacao'] = obj['licitacao_id'] and str(Licitacao.objects.get(pk=obj['licitacao_id'])) or '-'
            obj['url'] = 'http://189.124.139.92:8000/media/processos/{}.pdf'.format(obj['uuid'])
            obj['file'] = obj['arquivo'].split('/')[-1]
            file_path = '{}/{}'.format(settings.MEDIA_ROOT, obj['arquivo'])
            if os.path.exists(file_path):
                copy_medias.append('copy {} %disk%/digipro/dados/{}'.format(obj['arquivo'], obj['file']))
            else:
                nao_encontrados.append(obj['id'])
                copy_medias.append('echo "O arquivo do processo {} nao foi localizado!"'.format(obj['numero']))
            if len(l) > 1: l.append(', ')
            l.append(json.dumps(obj))

        l.append('];')
        dados.append(''.join(l))

        debug_file_path = '{}/digipro/dados/dados.js'.format(settings.MEDIA_ROOT)
        if settings.DEBUG:
            open(debug_file_path, 'w').write('\n'.join(dados))
        else:
            if os.path.exists(debug_file_path):
                os.unlink(debug_file_path)
        json_file_path = '{}.json'.format(os.path.join(settings.MEDIA_ROOT, slug))
        open(json_file_path, 'w').write('\n'.join(dados))

        bat_content = '''@echo off
set /p disk="Disco: "
rmdir /s /q %disk%\digipro
rmdir /s /q %disk%\digipro
Xcopy /E /I digipro %disk%\digipro
copy {}.json %disk%\digipro\dados\dados.json
{}
pause'''.format(slug, '\n'.join(copy_medias).replace('/', '\\'))
        cmd_file_path = '{}.bat'.format(os.path.join(settings.MEDIA_ROOT, slug))
        open(cmd_file_path, 'w').write(bat_content)

        sh_content = '''echo "Disco:"
read disk
cp -r digipro $disk/digipro
cp {}.json $disk/digipro/dados/dados.json
{}
cd $disk/digipro && ./open.sh&'''.format(slug, '\n'.join(copy_medias[0:2]).replace('copy ', 'cp ').replace('%/', '/').replace('%', '$'))
        sh_file_path = '{}.sh'.format(os.path.join(settings.MEDIA_ROOT, slug))
        open(sh_file_path, 'w').write(sh_content)
        return nao_encontrados


@role('cpf', scope='cliente')
class Mantenedor(models.Model):
    cliente = models.ForeignKey(Cliente, verbose_name='Cliente')
    cpf = models.CpfField('CPF', search=True)
    nome = models.CharField('Nome', search=True)

    class Meta:
        verbose_name = 'Mantenedor'
        verbose_name_plural = 'Mantenedores'
        menu = 'Usuários::Mantenedores', 'fa-users'
        can_admin = 'Administrador', 'Cadastrador'
        can_admin_by_unit = 'Mantenedor'
        list_display = 'cpf', 'nome'

    def __str__(self):
        return self.nome


@role('cpf', scope='cliente')
class Protocolo(models.Model):
    cliente = models.ForeignKey(Cliente, verbose_name='Cliente')
    cpf = models.CpfField('CPF', search=True)
    nome = models.CharField('Nome', search=True)

    class Meta:
        verbose_name = 'Protocolo'
        verbose_name_plural = 'Protocolos'
        menu = 'Usuários::Protocolos', 'fa-users'
        can_admin = 'Administrador', 'Cadastrador'
        can_admin_by_unit = 'Mantenedor'
        list_display = 'cpf', 'nome'

    def __str__(self):
        return self.nome


@role('cpf', scope='cliente')
class Visualizador(models.Model):
    cliente = models.ForeignKey(Cliente, verbose_name='Cliente')
    cpf = models.CpfField('CPF', search=True)
    nome = models.CharField('Nome', search=True)

    class Meta:
        verbose_name = 'Visualizador'
        verbose_name_plural = 'Visualizadores'
        menu = 'Usuários::Visualizadores', 'fa-users'
        can_admin = 'Administrador', 'Cadastrador'
        can_admin_by_unit = 'Mantenedor'
        icon = 'fa-eye'
        list_display = 'cpf', 'nome'

    def __str__(self):
        return self.nome


class Recurso(models.Model):
    cliente = models.ForeignKey(Cliente, verbose_name='Cliente', null=True)
    descricao = models.CharField('Descricão', search=True)

    class Meta:
        verbose_name = 'Recurso'
        verbose_name_plural = 'Recursos'
        can_admin = 'Administrador', 'Cadastrador'
        can_admin_by_unit = 'Mantenedor'
        list_lookups = 'cliente'
        menu = 'Cadastros Gerais::Recursos', 'fa-th'
        list_menu = 'Mantenedor'
        can_list_by_unit = 'Protocolo', 'Visualizador'
        list_display = 'descricao',

    def __str__(self):
        return self.descricao


class Genero(models.Model):
    cliente = models.ForeignKey(Cliente, verbose_name='Cliente', null=True)
    descricao = models.CharField('Descricão', search=True)

    class Meta:
        verbose_name = 'Gênero'
        verbose_name_plural = 'Gêneros'
        can_admin = 'Administrador', 'Cadastrador'
        can_admin_by_unit = 'Mantenedor'
        menu = 'Cadastros Gerais::Gêneros', 'fa-th'
        list_menu = 'Mantenedor'
        can_list_by_unit = 'Protocolo', 'Visualizador'
        list_display = 'descricao',

    def __str__(self):
        return self.descricao


class Secretaria(models.Model):
    cliente = models.ForeignKey(Cliente, verbose_name='Cliente', null=True)
    descricao = models.CharField('Descricão', search=True)

    class Meta:
        verbose_name = 'Secretaria'
        verbose_name_plural = 'Secretarias'
        can_admin_by_unit = 'Mantenedor'
        can_admin = 'Cadastrador', 'Administrador'
        menu = 'Cadastros Gerais::Secretarias', 'fa-th'
        list_menu = 'Mantenedor'
        can_list_by_unit = 'Protocolo', 'Visualizador'
        list_display = 'descricao',

    def __str__(self):
        return self.descricao


class Licitacao(models.Model):
    cliente = models.ForeignKey(Cliente, verbose_name='Cliente', null=True)
    descricao = models.CharField('Descricão', search=True)

    class Meta:
        verbose_name = 'Licitação'
        verbose_name_plural = 'Licitações'
        can_admin_by_unit = 'Mantenedor'
        can_admin = 'Cadastrador', 'Administrador'
        menu = 'Cadastros Gerais::Licitações', 'fa-th'
        list_menu = 'Mantenedor'
        can_list_by_unit = 'Protocolo', 'Visualizador'
        list_lookups = 'cliente',
        list_display = 'descricao',

    def __str__(self):
        return self.descricao


class ProcessoManager(models.DefaultManager):

    @meta('Total de Processos por Cliente', can_view=('Administrador', 'Cadastrador'), dashboard='center')
    def total_por_cliente(self):
        return self.all(self._user).count('cliente')

    @meta('Total de Processos por Ano', can_view=('Administrador', 'Cadastrador', 'Visualizador'), dashboard='center', formatter='bar_chart')
    def total_por_ano(self):
        anos = self.filter(ano__isnull=False).values_list('ano', flat=True).distinct()
        return self.filter(ano__in=anos).all(self._user).count('ano')

    @meta('Total de Processos por Ano/Recurso', can_view='Visualizador', dashboard='center')
    def total_por_recurso_ano(self):
        recursos = Recurso.objects.all(self._user).values_list('pk')
        return self.filter(recurso__in=recursos).count('recurso', 'ano')


class Processo(models.AsciiModel):

    ANO_CHOICES = [['', '']] + [[x, x] for x in range(1990, 2025)]
    MES_CHOICES = [['', '']] + [[x, x] for x in ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']]

    cliente = models.ForeignKey(Cliente, verbose_name='Cliente', null=True, blank=True, filter=True, search=['razao_social'])
    numero = models.CharField('Número', search=True)
    ano = models.IntegerField('Ano', choices=ANO_CHOICES, default=datetime.datetime.today().year, filter=True)
    mes = models.CharField('Mês', choices=MES_CHOICES, default='', blank=True)
    secretaria = models.ForeignKey(Secretaria, verbose_name='Secretaria', null=True, blank=True, form_filter=('cliente', 'cliente'), filter=True)
    genero = models.ForeignKey(Genero, verbose_name='Gênero', null=True, blank=True, form_filter=('cliente', 'cliente'), filter=True)
    objeto = models.TextField('Objeto', search=True)
    credor = models.ForeignKey(Pessoa, verbose_name='Credor', search=['nome'], lazy=True)
    numero_empenho = models.CharField('Nº do Empenho')
    recurso = models.ForeignKey(Recurso, form_filter=('cliente', 'cliente'), null=True, filter=True, verbose_name='Recurso')
    licitacao = models.ForeignKey(Licitacao, verbose_name='Licitação', null=True, form_filter=('cliente', 'cliente'), filter=True)
    numero_pasta = models.CharField('Localização Física')
    arquivo = models.FileField('Arquivo', upload_to=utils.get_file_path, blank=True, null=True, storage=DropboxStorage())
    tamanho_arquivo = models.IntegerField(verbose_name='Tamanho do Arquivo', null=True, exclude=True)
    qrcode = models.ImageField('QRCode', upload_to='qrcodes', exclude=True)
    numero_paginas = models.IntegerField('Número de Páginas', default=0, blank=True)
    uuid = models.CharField(exclude=True, default='', search=False)

    fieldsets = (('Dados Gerais', {'fields': ('cliente', ('numero', 'ano'), ('mes', 'secretaria'), 'credor', 'genero', 'objeto')}),
                 ('Informações Adicionais', {'fields': (('recurso', 'licitacao'), ('numero_empenho', 'numero_pasta'),)}),
                 ('Arquivo Digitalizado', {'fields': ('arquivo', 'numero_paginas', 'get_url', 'get_qrcode')}),
                 ('Protocolo', {'relations': ('movimentacao_set',)}),
                 )

    class Meta:
        verbose_name = 'Processo'
        verbose_name_plural = 'Processos'
        menu = 'Processos', 'fa-file'
        list_menu = 'Administrador', 'Cadastrador', 'Cliente', 'Protocolo'
        list_display = ['numero', 'ano', 'objeto', 'credor', 'numero_paginas', 'numero_pasta', 'get_tamanho_arquivo', 'get_qrcode']
        can_admin = 'Administrador', 'Cadastrador'
        can_admin_by_unit = 'Mantenedor',
        can_list_by_unit = 'Protocolo', 'Visualizador'
        list_lookups = 'cliente'
        icon = 'fa-file'
        list_per_page = 10
        list_template = 'processos.html'
        list_shortcut = 'Administrador', 'Cadastrador'
        list_xls = 'numero', 'ano', 'mes', 'objeto', 'credor', 'genero', 'recurso', 'licitacao', 'numero_empenho', 'numero_pasta', 'arquivo', 'numero_paginas', 'get_tamanho_arquivo'

    def __str__(self):
        return 'Processo %s' % self.numero

    def save(self, *args, **kwargs):
        if not self.uuid:
            self.uuid = uuid.uuid4()
        super(Processo, self).save()
        if self.arquivo:
            nome_arquivo = 'qrcodes/%s.gif' % self.uuid
            caminho_arquivo = '%s/%s' % (settings.MEDIA_ROOT, nome_arquivo)
            url = 'http://analisa.djangoplus.net/media/%s' % self.arquivo.name
            qrcode.make(url).save(caminho_arquivo, 'gif')
            self.qrcode = nome_arquivo
            super(Processo, self).save(*args, **kwargs)

            caminho_arquivo = os.path.join(settings.MEDIA_ROOT, self.arquivo.name)
            if os.path.exists(caminho_arquivo):
                self.tamanho_arquivo = os.stat(caminho_arquivo).st_size
            else:
                self.tamanho_arquivo = 0
            super(Processo, self).save()

    def delete(self, *args, **kwargs):
        if os.path.exists(self.arquivo.path):
            os.unlink(self.arquivo.path)
        super(Processo, self).delete(*args, **kwargs)

    @meta('Tamanho')
    def get_tamanho_arquivo(self, recalcular=False):
        unidade = 'bytes'
        tamanho_arquivo = self.tamanho_arquivo
        if tamanho_arquivo > 1024:
            tamanho_arquivo = tamanho_arquivo / 1024
            unidade = 'Kb'
        if tamanho_arquivo > 1024:
            tamanho_arquivo = tamanho_arquivo / 1024
            unidade = 'Mb'
        return '{:.2f} {}'.format(tamanho_arquivo, unidade).replace('.', ',')

    @meta('QRCode', formatter='qrcode')
    def get_qrcode(self):
        return self.get_url()

    @meta('URL')
    def get_url(self):
        return 'http://analisarn.djangoplus.net:8000/media/%s' % self.arquivo.name


class MovimentacaoManager(models.DefaultManager):

    @subset('Não Devolvidas')
    def nao_devolvidas(self):
        return self.filter(data_devolucao__isnull=True)

    @subset('Devolvidas')
    def devolvidas(self):
        return self.exclude(data_devolucao__isnull=True)


class Movimentacao(models.Model):
    processo = models.ForeignKey(Processo, verbose_name='Processo', composition=True)
    data = models.DateField('Data da Retirada')
    motivo = models.TextField('Motivo')
    nome_solicitante = models.CharField('Nome')
    cpf_solicitante = models.CpfField('CPF')
    data_devolucao = models.DateField('Data da Devolução', null=True, exclude=True)

    fieldsets = (('Dados Gerais', {'fields': ('processo', 'data', 'motivo',)}),
                 ('Dados do Solicitante', {'fields': (('nome_solicitante', 'cpf_solicitante'),)}),
                 ('Dados da Entregua', {'fields': ('data_devolucao',)}),)

    class Meta:
        verbose_name = 'Movimentação'
        verbose_name_plural = 'Movimentações'
        menu = 'Movimentações'
        order_by = ('-data',)
        verbose_female = True
        can_add_by_unit = 'Protocolo'
        can_edit_by_unit = 'Protocolo'
        can_list_by_unit = 'Protocolo'
        can_admin = 'Administrador'
        list_lookups = 'processo__cliente'
        list_display = 'data', 'motivo', 'nome_solicitante', 'cpf_solicitante', 'data_devolucao'

    def __str__(self):
        return 'Movimentação %s' % self.pk

    @action('Registrar Devolução', can_execute='Protocolo', inline=True, condition='is_pendente')
    def registrar_devolucao(self, data_devolucao):
        self.save()

    @action('Cancelar Devolução', can_execute='Protocolo', inline=True, condition='is_entegue')
    def cancelar_devolucao(self):
        self.data_devolucao = None
        self.save()

    def is_pendente(self):
        return self.data_devolucao is None

    def is_entegue(self):
        return self.data_devolucao is not None

    def clean(self):
        if self.processo and not self.processo.cliente.pode_usuar_protocolo():
            raise ValidationError('O módulo de protocolo encontra-se intativo. \
            Por favor, solicite sua ativação à Analisa. ')

        if not self.pk:
            if Movimentacao.objects.filter(processo=self.processo, data_devolucao__isnull=True).exists():
                raise ValidationError('Esse processo foi retirado e ainda não foi devolvido')

            qs = Movimentacao.objects.filter(processo=self.processo).order_by('-data_devolucao').values_list('data_devolucao', flat=True)
            if qs.exists() and qs[0] > self.data:
                raise ValidationError('A data da retirada deve ser maior ou igual a data da última entrega.')
        else:
            if self.data_devolucao and self.data_devolucao < self.data:
                raise ValidationError('A data da devolução deve ser maior ou igual a data da última retirada.')


class LicensaUsoProtocolo(models.Model):
    cliente = models.ForeignKey(Cliente, verbose_name='Cliente', null=True)
    data_ativacao = models.DateField(verbose_name='Data da Ativação', null=True, default=datetime.date.today)
    validade = models.DateField('Validade')

    class Meta:
        verbose_name = 'Licença de Uso'
        verbose_name_plural = 'Licenças de Uso'
        can_delete = 'Administrador'

